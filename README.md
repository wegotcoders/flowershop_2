# Flowershop video #2

## Here's a couple arrays you can experiment with

Some spring flowers:
```
spring = ["Amaryllis", "Anemone", "Azalea", "Allium", "Bird of Paradise", "Begonia", "Bluebell", "Boronia", "Bloodroot", "Calla Lily", "Cornflower", "Cosmos", "Crocus", "Camellia", "Dahlia", "Freesia", "Gardenia", "Glory Lilies", "Geranium", "Gerbera", "Ghost Flower", "Glory of the Snow", "Grape Hyacinth", "Heath", "Hyacinth", "Hollyhock", "Iris", "Jasmine", "Jack in the Pulpit", "Lilium", "Lily of the Nile", "Larkspur", "Lilac", "Lisianthus", "Marigold", "Magnolia", "Orchid", "Peony", "Petunia", "Rose", "Statice", "Sweet Pea", "Tulip", "Tiger Lily", "Zinnia"]
```

And some summer flowers:
```
summer = %w(Aster Allium American Persimmon Bouvardia Baby's\ Breath Bird\ of\ Paradise Bellflower Blazing\ Star Carnation Chrysanthemum Cockscomb Calla\ Lily Christmas\ Bells Cosmos Casablanca\ Lily Chilean\ Jasmine Dahlia Daisy Delphinium Dianthus Didiscus Freesia Frangipani Gladiolus Geranium Garden\ Roses Grevillea Gerbera Gardenia Glory\ Lilies Gymnadenia Hydrangea Heliconia Hypericum Iris Impatiens Impatiens Jack\ in\ the\ Pulpit Lisianthus Lily\ of\ the\ Nile Lavender Lady's\ Mantle Lilac Madagascar\ Jasmine Marigold Naked\ lady\ flowers Orchid Pansy Peony Pink\ primrose Queen\ Anne's\ Lace Sunflower Statice Stephanotis Sweet\ Pea Stargazer\ Lily Sand\ Lily Sea\ Daffodil Tuberose Vinca\ Rosea Venus's\ Car)
```
